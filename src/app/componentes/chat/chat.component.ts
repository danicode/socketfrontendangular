import { Component, OnInit } from '@angular/core';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  private client: Client;
  conectado = false;
  mensaje: any = {};
  mensajes: any[] = [];
  escribiendo: string;
  clienteId: string;
  constructor() { }

  ngOnInit(): void {
    this.clienteId = 'id-' + new Date().getUTCMilliseconds() + '-' + Math.random().toString(36).substr(2);
    this.client = new Client();
    this.client.webSocketFactory = () => {
      return new SockJS('http://localhost:7070/chatwebsocket');
    };
    this.client.onConnect =  (frame) => {
      console.log('Conectados: ' + this.client.connected + ' : ' + frame);
      this.client.subscribe('/chat/mensaje',
      (e) => {
        const m: any = JSON.parse(e.body);
        if (m.tipo === 'NUEVO_USUARIO' && this.mensaje.username === m.username) {
          this.mensaje.color = m.color;
        }
        this.mensajes.push(m);
      });
      this.client.subscribe('/chat/escribiendo',
      (e) => {
        console.log('subscribe escribiendo');
        this.escribiendo = e.body;
        setTimeout(() => {
          this.escribiendo = '';
        }, 2000);
      });
      this.client.subscribe('/chat/historial/' + this.clienteId,
      (e) => {
        const historial = JSON.parse(e.body);
        this.mensajes = historial.reverse();
      });
      this.client.publish({destination: '/app/historial', body: this.clienteId});
      this.mensaje.tipo = 'NUEVO_USUARIO';
      this.client.publish({destination: '/app/mensaje', body: JSON.stringify(this.mensaje) });

      this.conectado = true;
    };

    this.client.onDisconnect = (frame) => {
      console.log('Desconectados: ' + !this.client.connected + ' : ' + frame);
      this.conectado = false;
    };
  }
  conectar() {
    this.client.activate();
  }

  desconectar() {
    console.log(this.mensaje);
    this.mensaje.username = '';
    this.mensaje.tipo = '';
    this.client.deactivate();
  }

  enviarMensaje() {
    this.mensaje.tipo = 'MENSAJE';
    console.log(this.mensaje);

    this.client.publish({destination: '/app/mensaje', body: JSON.stringify(this.mensaje) });
    this.mensaje.texto = '';
  }
  escribiendoEvento() {
    this.client.publish({destination: '/app/escribiendo', body: this.mensaje.username});
  }
}
